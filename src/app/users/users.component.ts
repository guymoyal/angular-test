import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { User } from './user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  public users = []
  public groups = []
  public selectedGroup: string

  constructor(private _userService: UserService) {}

  ngOnInit() {
    var group = ['Admin', 'Editor', 'Developer']
    this._userService.getUsers()
      .subscribe(data => {
        data.map(item => {
          let index = Math.floor(Math.random() * Math.floor(3))
          item['group'] = group[index]
        })
        this.users = data

        this.users.map(item => {
          if (!this.groups.includes(item.group)) {
            this.groups.push(item.group)
          }
        })
        console.log(this.users)
      }


    )
  }

}
