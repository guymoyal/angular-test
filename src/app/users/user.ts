export interface User {
  id: number,
  name: string,
  username: string,
  group: string,
}
